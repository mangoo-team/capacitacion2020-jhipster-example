package com.mangoodev.com.service;

import com.mangoodev.com.domain.Localidad;
import com.mangoodev.com.repository.LocalidadRepository;
import com.mangoodev.com.service.dto.LocalidadDTO;
import com.mangoodev.com.service.mapper.LocalidadMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Localidad}.
 */
@Service
@Transactional
public class LocalidadService {

    private final Logger log = LoggerFactory.getLogger(LocalidadService.class);

    private final LocalidadRepository localidadRepository;

    private final LocalidadMapper localidadMapper;

    public LocalidadService(LocalidadRepository localidadRepository, LocalidadMapper localidadMapper) {
        this.localidadRepository = localidadRepository;
        this.localidadMapper = localidadMapper;
    }

    /**
     * Save a localidad.
     *
     * @param localidadDTO the entity to save.
     * @return the persisted entity.
     */
    public LocalidadDTO save(LocalidadDTO localidadDTO) {
        log.debug("Request to save Localidad : {}", localidadDTO);
        Localidad localidad = localidadMapper.toEntity(localidadDTO);
        localidad = localidadRepository.save(localidad);
        return localidadMapper.toDto(localidad);
    }

    /**
     * Get all the localidads.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<LocalidadDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Localidads");
        return localidadRepository.findAll(pageable)
            .map(localidadMapper::toDto);
    }


    /**
     * Get one localidad by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<LocalidadDTO> findOne(Long id) {
        log.debug("Request to get Localidad : {}", id);
        return localidadRepository.findById(id)
            .map(localidadMapper::toDto);
    }

    /**
     * Delete the localidad by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Localidad : {}", id);
        localidadRepository.deleteById(id);
    }
}
