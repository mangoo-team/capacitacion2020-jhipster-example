package com.mangoodev.com.service.mapper;


import com.mangoodev.com.domain.*;
import com.mangoodev.com.service.dto.LocalidadDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Localidad} and its DTO {@link LocalidadDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProvinciaMapper.class})
public interface LocalidadMapper extends EntityMapper<LocalidadDTO, Localidad> {

    @Mapping(source = "provincia.id", target = "provinciaId")
    @Mapping(source = "provincia.nombre", target = "provinciaNombre")
    LocalidadDTO toDto(Localidad localidad);

    @Mapping(source = "provinciaId", target = "provincia")
    Localidad toEntity(LocalidadDTO localidadDTO);

    default Localidad fromId(Long id) {
        if (id == null) {
            return null;
        }
        Localidad localidad = new Localidad();
        localidad.setId(id);
        return localidad;
    }
}
