package com.mangoodev.com.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.mangoodev.com.domain.Localidad} entity.
 */
public class LocalidadDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 250)
    private String nombre;

    @NotNull
    @Size(max = 20)
    private String codigoPostal;


    private Long provinciaId;

    private String provinciaNombre;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public Long getProvinciaId() {
        return provinciaId;
    }

    public void setProvinciaId(Long provinciaId) {
        this.provinciaId = provinciaId;
    }

    public String getProvinciaNombre() {
        return provinciaNombre;
    }

    public void setProvinciaNombre(String provinciaNombre) {
        this.provinciaNombre = provinciaNombre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LocalidadDTO)) {
            return false;
        }

        return id != null && id.equals(((LocalidadDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocalidadDTO{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", codigoPostal='" + getCodigoPostal() + "'" +
            ", provinciaId=" + getProvinciaId() +
            ", provinciaNombre='" + getProvinciaNombre() + "'" +
            "}";
    }
}
