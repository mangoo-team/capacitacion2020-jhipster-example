export interface IProvincia {
  id?: number;
  nombre?: string;
}

export class Provincia implements IProvincia {
  constructor(public id?: number, public nombre?: string) {}
}
