export interface ILocalidad {
  id?: number;
  nombre?: string;
  codigoPostal?: string;
  provinciaNombre?: string;
  provinciaId?: number;
}

export class Localidad implements ILocalidad {
  constructor(
    public id?: number,
    public nombre?: string,
    public codigoPostal?: string,
    public provinciaNombre?: string,
    public provinciaId?: number
  ) {}
}
