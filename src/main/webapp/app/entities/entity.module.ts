import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'provincia',
        loadChildren: () => import('./provincia/provincia.module').then(m => m.ExampleProvinciaModule),
      },
      {
        path: 'localidad',
        loadChildren: () => import('./localidad/localidad.module').then(m => m.ExampleLocalidadModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class ExampleEntityModule {}
