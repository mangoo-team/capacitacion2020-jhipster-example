import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ILocalidad, Localidad } from 'app/shared/model/localidad.model';
import { LocalidadService } from './localidad.service';
import { IProvincia } from 'app/shared/model/provincia.model';
import { ProvinciaService } from 'app/entities/provincia/provincia.service';

@Component({
  selector: 'jhi-localidad-update',
  templateUrl: './localidad-update.component.html',
})
export class LocalidadUpdateComponent implements OnInit {
  isSaving = false;
  provincias: IProvincia[] = [];

  editForm = this.fb.group({
    id: [],
    nombre: [null, [Validators.required, Validators.maxLength(250)]],
    codigoPostal: [null, [Validators.required, Validators.maxLength(20)]],
    provinciaId: [null, Validators.required],
  });

  constructor(
    protected localidadService: LocalidadService,
    protected provinciaService: ProvinciaService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ localidad }) => {
      this.updateForm(localidad);

      this.provinciaService.query().subscribe((res: HttpResponse<IProvincia[]>) => (this.provincias = res.body || []));
    });
  }

  updateForm(localidad: ILocalidad): void {
    this.editForm.patchValue({
      id: localidad.id,
      nombre: localidad.nombre,
      codigoPostal: localidad.codigoPostal,
      provinciaId: localidad.provinciaId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const localidad = this.createFromForm();
    if (localidad.id !== undefined) {
      this.subscribeToSaveResponse(this.localidadService.update(localidad));
    } else {
      this.subscribeToSaveResponse(this.localidadService.create(localidad));
    }
  }

  private createFromForm(): ILocalidad {
    return {
      ...new Localidad(),
      id: this.editForm.get(['id'])!.value,
      nombre: this.editForm.get(['nombre'])!.value,
      codigoPostal: this.editForm.get(['codigoPostal'])!.value,
      provinciaId: this.editForm.get(['provinciaId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILocalidad>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IProvincia): any {
    return item.id;
  }
}
