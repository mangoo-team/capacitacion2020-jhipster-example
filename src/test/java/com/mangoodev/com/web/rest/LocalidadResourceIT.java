package com.mangoodev.com.web.rest;

import com.mangoodev.com.ExampleApp;
import com.mangoodev.com.domain.Localidad;
import com.mangoodev.com.domain.Provincia;
import com.mangoodev.com.repository.LocalidadRepository;
import com.mangoodev.com.service.LocalidadService;
import com.mangoodev.com.service.dto.LocalidadDTO;
import com.mangoodev.com.service.mapper.LocalidadMapper;
import com.mangoodev.com.service.dto.LocalidadCriteria;
import com.mangoodev.com.service.LocalidadQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LocalidadResource} REST controller.
 */
@SpringBootTest(classes = ExampleApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class LocalidadResourceIT {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    private static final String DEFAULT_CODIGO_POSTAL = "AAAAAAAAAA";
    private static final String UPDATED_CODIGO_POSTAL = "BBBBBBBBBB";

    @Autowired
    private LocalidadRepository localidadRepository;

    @Autowired
    private LocalidadMapper localidadMapper;

    @Autowired
    private LocalidadService localidadService;

    @Autowired
    private LocalidadQueryService localidadQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLocalidadMockMvc;

    private Localidad localidad;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Localidad createEntity(EntityManager em) {
        Localidad localidad = new Localidad()
            .nombre(DEFAULT_NOMBRE)
            .codigoPostal(DEFAULT_CODIGO_POSTAL);
        // Add required entity
        Provincia provincia;
        if (TestUtil.findAll(em, Provincia.class).isEmpty()) {
            provincia = ProvinciaResourceIT.createEntity(em);
            em.persist(provincia);
            em.flush();
        } else {
            provincia = TestUtil.findAll(em, Provincia.class).get(0);
        }
        localidad.setProvincia(provincia);
        return localidad;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Localidad createUpdatedEntity(EntityManager em) {
        Localidad localidad = new Localidad()
            .nombre(UPDATED_NOMBRE)
            .codigoPostal(UPDATED_CODIGO_POSTAL);
        // Add required entity
        Provincia provincia;
        if (TestUtil.findAll(em, Provincia.class).isEmpty()) {
            provincia = ProvinciaResourceIT.createUpdatedEntity(em);
            em.persist(provincia);
            em.flush();
        } else {
            provincia = TestUtil.findAll(em, Provincia.class).get(0);
        }
        localidad.setProvincia(provincia);
        return localidad;
    }

    @BeforeEach
    public void initTest() {
        localidad = createEntity(em);
    }

    @Test
    @Transactional
    public void createLocalidad() throws Exception {
        int databaseSizeBeforeCreate = localidadRepository.findAll().size();
        // Create the Localidad
        LocalidadDTO localidadDTO = localidadMapper.toDto(localidad);
        restLocalidadMockMvc.perform(post("/api/localidads")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localidadDTO)))
            .andExpect(status().isCreated());

        // Validate the Localidad in the database
        List<Localidad> localidadList = localidadRepository.findAll();
        assertThat(localidadList).hasSize(databaseSizeBeforeCreate + 1);
        Localidad testLocalidad = localidadList.get(localidadList.size() - 1);
        assertThat(testLocalidad.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testLocalidad.getCodigoPostal()).isEqualTo(DEFAULT_CODIGO_POSTAL);
    }

    @Test
    @Transactional
    public void createLocalidadWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = localidadRepository.findAll().size();

        // Create the Localidad with an existing ID
        localidad.setId(1L);
        LocalidadDTO localidadDTO = localidadMapper.toDto(localidad);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLocalidadMockMvc.perform(post("/api/localidads")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localidadDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Localidad in the database
        List<Localidad> localidadList = localidadRepository.findAll();
        assertThat(localidadList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNombreIsRequired() throws Exception {
        int databaseSizeBeforeTest = localidadRepository.findAll().size();
        // set the field null
        localidad.setNombre(null);

        // Create the Localidad, which fails.
        LocalidadDTO localidadDTO = localidadMapper.toDto(localidad);


        restLocalidadMockMvc.perform(post("/api/localidads")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localidadDTO)))
            .andExpect(status().isBadRequest());

        List<Localidad> localidadList = localidadRepository.findAll();
        assertThat(localidadList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodigoPostalIsRequired() throws Exception {
        int databaseSizeBeforeTest = localidadRepository.findAll().size();
        // set the field null
        localidad.setCodigoPostal(null);

        // Create the Localidad, which fails.
        LocalidadDTO localidadDTO = localidadMapper.toDto(localidad);


        restLocalidadMockMvc.perform(post("/api/localidads")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localidadDTO)))
            .andExpect(status().isBadRequest());

        List<Localidad> localidadList = localidadRepository.findAll();
        assertThat(localidadList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLocalidads() throws Exception {
        // Initialize the database
        localidadRepository.saveAndFlush(localidad);

        // Get all the localidadList
        restLocalidadMockMvc.perform(get("/api/localidads?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(localidad.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE)))
            .andExpect(jsonPath("$.[*].codigoPostal").value(hasItem(DEFAULT_CODIGO_POSTAL)));
    }
    
    @Test
    @Transactional
    public void getLocalidad() throws Exception {
        // Initialize the database
        localidadRepository.saveAndFlush(localidad);

        // Get the localidad
        restLocalidadMockMvc.perform(get("/api/localidads/{id}", localidad.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(localidad.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE))
            .andExpect(jsonPath("$.codigoPostal").value(DEFAULT_CODIGO_POSTAL));
    }


    @Test
    @Transactional
    public void getLocalidadsByIdFiltering() throws Exception {
        // Initialize the database
        localidadRepository.saveAndFlush(localidad);

        Long id = localidad.getId();

        defaultLocalidadShouldBeFound("id.equals=" + id);
        defaultLocalidadShouldNotBeFound("id.notEquals=" + id);

        defaultLocalidadShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLocalidadShouldNotBeFound("id.greaterThan=" + id);

        defaultLocalidadShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLocalidadShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllLocalidadsByNombreIsEqualToSomething() throws Exception {
        // Initialize the database
        localidadRepository.saveAndFlush(localidad);

        // Get all the localidadList where nombre equals to DEFAULT_NOMBRE
        defaultLocalidadShouldBeFound("nombre.equals=" + DEFAULT_NOMBRE);

        // Get all the localidadList where nombre equals to UPDATED_NOMBRE
        defaultLocalidadShouldNotBeFound("nombre.equals=" + UPDATED_NOMBRE);
    }

    @Test
    @Transactional
    public void getAllLocalidadsByNombreIsNotEqualToSomething() throws Exception {
        // Initialize the database
        localidadRepository.saveAndFlush(localidad);

        // Get all the localidadList where nombre not equals to DEFAULT_NOMBRE
        defaultLocalidadShouldNotBeFound("nombre.notEquals=" + DEFAULT_NOMBRE);

        // Get all the localidadList where nombre not equals to UPDATED_NOMBRE
        defaultLocalidadShouldBeFound("nombre.notEquals=" + UPDATED_NOMBRE);
    }

    @Test
    @Transactional
    public void getAllLocalidadsByNombreIsInShouldWork() throws Exception {
        // Initialize the database
        localidadRepository.saveAndFlush(localidad);

        // Get all the localidadList where nombre in DEFAULT_NOMBRE or UPDATED_NOMBRE
        defaultLocalidadShouldBeFound("nombre.in=" + DEFAULT_NOMBRE + "," + UPDATED_NOMBRE);

        // Get all the localidadList where nombre equals to UPDATED_NOMBRE
        defaultLocalidadShouldNotBeFound("nombre.in=" + UPDATED_NOMBRE);
    }

    @Test
    @Transactional
    public void getAllLocalidadsByNombreIsNullOrNotNull() throws Exception {
        // Initialize the database
        localidadRepository.saveAndFlush(localidad);

        // Get all the localidadList where nombre is not null
        defaultLocalidadShouldBeFound("nombre.specified=true");

        // Get all the localidadList where nombre is null
        defaultLocalidadShouldNotBeFound("nombre.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocalidadsByNombreContainsSomething() throws Exception {
        // Initialize the database
        localidadRepository.saveAndFlush(localidad);

        // Get all the localidadList where nombre contains DEFAULT_NOMBRE
        defaultLocalidadShouldBeFound("nombre.contains=" + DEFAULT_NOMBRE);

        // Get all the localidadList where nombre contains UPDATED_NOMBRE
        defaultLocalidadShouldNotBeFound("nombre.contains=" + UPDATED_NOMBRE);
    }

    @Test
    @Transactional
    public void getAllLocalidadsByNombreNotContainsSomething() throws Exception {
        // Initialize the database
        localidadRepository.saveAndFlush(localidad);

        // Get all the localidadList where nombre does not contain DEFAULT_NOMBRE
        defaultLocalidadShouldNotBeFound("nombre.doesNotContain=" + DEFAULT_NOMBRE);

        // Get all the localidadList where nombre does not contain UPDATED_NOMBRE
        defaultLocalidadShouldBeFound("nombre.doesNotContain=" + UPDATED_NOMBRE);
    }


    @Test
    @Transactional
    public void getAllLocalidadsByCodigoPostalIsEqualToSomething() throws Exception {
        // Initialize the database
        localidadRepository.saveAndFlush(localidad);

        // Get all the localidadList where codigoPostal equals to DEFAULT_CODIGO_POSTAL
        defaultLocalidadShouldBeFound("codigoPostal.equals=" + DEFAULT_CODIGO_POSTAL);

        // Get all the localidadList where codigoPostal equals to UPDATED_CODIGO_POSTAL
        defaultLocalidadShouldNotBeFound("codigoPostal.equals=" + UPDATED_CODIGO_POSTAL);
    }

    @Test
    @Transactional
    public void getAllLocalidadsByCodigoPostalIsNotEqualToSomething() throws Exception {
        // Initialize the database
        localidadRepository.saveAndFlush(localidad);

        // Get all the localidadList where codigoPostal not equals to DEFAULT_CODIGO_POSTAL
        defaultLocalidadShouldNotBeFound("codigoPostal.notEquals=" + DEFAULT_CODIGO_POSTAL);

        // Get all the localidadList where codigoPostal not equals to UPDATED_CODIGO_POSTAL
        defaultLocalidadShouldBeFound("codigoPostal.notEquals=" + UPDATED_CODIGO_POSTAL);
    }

    @Test
    @Transactional
    public void getAllLocalidadsByCodigoPostalIsInShouldWork() throws Exception {
        // Initialize the database
        localidadRepository.saveAndFlush(localidad);

        // Get all the localidadList where codigoPostal in DEFAULT_CODIGO_POSTAL or UPDATED_CODIGO_POSTAL
        defaultLocalidadShouldBeFound("codigoPostal.in=" + DEFAULT_CODIGO_POSTAL + "," + UPDATED_CODIGO_POSTAL);

        // Get all the localidadList where codigoPostal equals to UPDATED_CODIGO_POSTAL
        defaultLocalidadShouldNotBeFound("codigoPostal.in=" + UPDATED_CODIGO_POSTAL);
    }

    @Test
    @Transactional
    public void getAllLocalidadsByCodigoPostalIsNullOrNotNull() throws Exception {
        // Initialize the database
        localidadRepository.saveAndFlush(localidad);

        // Get all the localidadList where codigoPostal is not null
        defaultLocalidadShouldBeFound("codigoPostal.specified=true");

        // Get all the localidadList where codigoPostal is null
        defaultLocalidadShouldNotBeFound("codigoPostal.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocalidadsByCodigoPostalContainsSomething() throws Exception {
        // Initialize the database
        localidadRepository.saveAndFlush(localidad);

        // Get all the localidadList where codigoPostal contains DEFAULT_CODIGO_POSTAL
        defaultLocalidadShouldBeFound("codigoPostal.contains=" + DEFAULT_CODIGO_POSTAL);

        // Get all the localidadList where codigoPostal contains UPDATED_CODIGO_POSTAL
        defaultLocalidadShouldNotBeFound("codigoPostal.contains=" + UPDATED_CODIGO_POSTAL);
    }

    @Test
    @Transactional
    public void getAllLocalidadsByCodigoPostalNotContainsSomething() throws Exception {
        // Initialize the database
        localidadRepository.saveAndFlush(localidad);

        // Get all the localidadList where codigoPostal does not contain DEFAULT_CODIGO_POSTAL
        defaultLocalidadShouldNotBeFound("codigoPostal.doesNotContain=" + DEFAULT_CODIGO_POSTAL);

        // Get all the localidadList where codigoPostal does not contain UPDATED_CODIGO_POSTAL
        defaultLocalidadShouldBeFound("codigoPostal.doesNotContain=" + UPDATED_CODIGO_POSTAL);
    }


    @Test
    @Transactional
    public void getAllLocalidadsByProvinciaIsEqualToSomething() throws Exception {
        // Get already existing entity
        Provincia provincia = localidad.getProvincia();
        localidadRepository.saveAndFlush(localidad);
        Long provinciaId = provincia.getId();

        // Get all the localidadList where provincia equals to provinciaId
        defaultLocalidadShouldBeFound("provinciaId.equals=" + provinciaId);

        // Get all the localidadList where provincia equals to provinciaId + 1
        defaultLocalidadShouldNotBeFound("provinciaId.equals=" + (provinciaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLocalidadShouldBeFound(String filter) throws Exception {
        restLocalidadMockMvc.perform(get("/api/localidads?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(localidad.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE)))
            .andExpect(jsonPath("$.[*].codigoPostal").value(hasItem(DEFAULT_CODIGO_POSTAL)));

        // Check, that the count call also returns 1
        restLocalidadMockMvc.perform(get("/api/localidads/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLocalidadShouldNotBeFound(String filter) throws Exception {
        restLocalidadMockMvc.perform(get("/api/localidads?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLocalidadMockMvc.perform(get("/api/localidads/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingLocalidad() throws Exception {
        // Get the localidad
        restLocalidadMockMvc.perform(get("/api/localidads/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLocalidad() throws Exception {
        // Initialize the database
        localidadRepository.saveAndFlush(localidad);

        int databaseSizeBeforeUpdate = localidadRepository.findAll().size();

        // Update the localidad
        Localidad updatedLocalidad = localidadRepository.findById(localidad.getId()).get();
        // Disconnect from session so that the updates on updatedLocalidad are not directly saved in db
        em.detach(updatedLocalidad);
        updatedLocalidad
            .nombre(UPDATED_NOMBRE)
            .codigoPostal(UPDATED_CODIGO_POSTAL);
        LocalidadDTO localidadDTO = localidadMapper.toDto(updatedLocalidad);

        restLocalidadMockMvc.perform(put("/api/localidads")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localidadDTO)))
            .andExpect(status().isOk());

        // Validate the Localidad in the database
        List<Localidad> localidadList = localidadRepository.findAll();
        assertThat(localidadList).hasSize(databaseSizeBeforeUpdate);
        Localidad testLocalidad = localidadList.get(localidadList.size() - 1);
        assertThat(testLocalidad.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testLocalidad.getCodigoPostal()).isEqualTo(UPDATED_CODIGO_POSTAL);
    }

    @Test
    @Transactional
    public void updateNonExistingLocalidad() throws Exception {
        int databaseSizeBeforeUpdate = localidadRepository.findAll().size();

        // Create the Localidad
        LocalidadDTO localidadDTO = localidadMapper.toDto(localidad);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLocalidadMockMvc.perform(put("/api/localidads")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(localidadDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Localidad in the database
        List<Localidad> localidadList = localidadRepository.findAll();
        assertThat(localidadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLocalidad() throws Exception {
        // Initialize the database
        localidadRepository.saveAndFlush(localidad);

        int databaseSizeBeforeDelete = localidadRepository.findAll().size();

        // Delete the localidad
        restLocalidadMockMvc.perform(delete("/api/localidads/{id}", localidad.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Localidad> localidadList = localidadRepository.findAll();
        assertThat(localidadList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
