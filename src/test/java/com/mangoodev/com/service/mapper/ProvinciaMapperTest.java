package com.mangoodev.com.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ProvinciaMapperTest {

    private ProvinciaMapper provinciaMapper;

    @BeforeEach
    public void setUp() {
        provinciaMapper = new ProvinciaMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(provinciaMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(provinciaMapper.fromId(null)).isNull();
    }
}
