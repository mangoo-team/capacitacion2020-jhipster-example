package com.mangoodev.com.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class LocalidadMapperTest {

    private LocalidadMapper localidadMapper;

    @BeforeEach
    public void setUp() {
        localidadMapper = new LocalidadMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(localidadMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(localidadMapper.fromId(null)).isNull();
    }
}
